# README #

## Summary ##


## Setting up ##

configure and run `set_env.sh` e.g. `source ./set_env.sh`

## Execution ##

### The following cronjob will execute the morpheus dream generator script at minute 0 past every hour from 6AM to 11PM
`0 6-23 * * * cd /home/ec2-user/morpheusdev/morpheus-automation && source ./set_env.sh && /home/ec2-user/.pyenv/versions/3.7.3/envs/morpheus-env-3.7.3/bin/python generate_drafts.py`

## API keys ##

You need to add the required keys to `set_env.sh` and run it before use.

## Retraining ##

If you have a partially-trained GPT-2 model and want to continue finetuning it, you can set overwrite=True to finetune, which will continue training and remove the previous iteration of the model without creating a duplicate copy. This can be especially useful for transfer learning (e.g. heavily finetune GPT-2 on one dataset, then finetune on other dataset to get a "merging" of both datasets).

## Contact ##


## TODO

* retrain with better start/stop data to get tweet-sized dreams
* tweak parameters like temperature, top_k, top_p for best results
* test local
* add `export ~/set_env.sh` to `~/.bashrc` in cloud
* configure cronjob
* test in cloud
* add cloudwatch logging if it's free