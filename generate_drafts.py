from random import randint
import os
import sys
import logging
from datetime import datetime
from PIL import Image, ImageDraw, ImageFont
import gpt_2_simple as gpt2
import twitter
import hashlib
import textwrap

sess = gpt2.start_tf_sess()
gpt2.load_gpt2(sess, run_name=os.environ.get("MODEL_ID"))
api = twitter.Api(consumer_key=os.environ.get("CONSUMER_KEY"),
                  consumer_secret=os.environ.get("CONSUMER_SECRET"),
                  access_token_key=os.environ.get("ACCESS_TOKEN_KEY"),
                  access_token_secret=os.environ.get("ACCESS_TOKEN_SECRET"),
                  sleep_on_rate_limit=True)

os.makedirs('./archive', exist_ok = True)
os.makedirs('./archive/images', exist_ok = True)
os.makedirs('./logs', exist_ok = True)

rootLogger = logging.getLogger('morpheusbot')
rootLogger.setLevel('INFO')

# stdout logging
printHandler = logging.StreamHandler(sys.stdout)
printHandler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
printHandler.setFormatter(formatter)
rootLogger.addHandler(printHandler)
# file logging
fileHandler = logging.FileHandler(f"./logs/run-{datetime.now().strftime('%d-%m-%Y_%H-%M-%S')}.log")
fileHandler.setLevel(logging.DEBUG)
fileHandler.setFormatter(formatter)
rootLogger.addHandler(fileHandler)


DREAM_THRESHOLD = int(sys.argv[1]) if len(sys.argv) > 1 else 10
RETRY_LIMIT = 3

good_dreams = []

def teardown():
  # close handlers
  for handler in rootLogger.handlers:
      handler.close()
      rootLogger.removeFilter(handler)

def good(dream):
  return len(dream) <= 1500 and dream[-1] == '.'

# save dream to today's archive
def archive_dream(dream):
  path = './archive/'+datetime.today().strftime('%d-%m-%Y')+'.txt'
  rootLogger.info(f"Saving dream to {path}")
  with open(path, 'a', encoding='utf-8') as f:
    f.write(str(dream+'\n'))

def save_image(dream):
  A = 4
  B = 3
  W = 1100
  H = 628
  fontsize = 24
  text = str(dream)
  paragraph = textwrap.wrap(text, width=90)
  img = Image.new('RGBA', (W*A, H*A), color = 'white')
  d = ImageDraw.Draw(img)
  font = ImageFont.truetype('./fonts/HelveticaNeue-Medium.otf', fontsize*A)
  current_h, pad = (H*A / 2) - (len(paragraph) * fontsize*B), 15*A
  for line in paragraph:
      w, h = d.textsize(line, font=font)
      d.text(((W*A - w) / 2, current_h), line, font=font, fill='black')
      current_h += h + pad
  img = img.resize((W, H), Image.ANTIALIAS)
  fp = './archive/images/'+hashlib.md5(text.encode()).hexdigest()+'.png'
  img.save(fp,'PNG')
  return fp

def dream():
  while len(good_dreams) < DREAM_THRESHOLD:
    coin_flip = randint(0,1)
    if (coin_flip == 0):
      token_count = randint(35, 50)
    else:
      token_count = randint(50, 500)
    rootLogger.info(f"Sampling at {token_count} tokens")
    fresh_dreams = gpt2.generate(sess,
                                run_name='run-v1e',
                                length=token_count,
                                temperature=0.9,
                                nsamples=5,
                                batch_size=5,
                                return_as_list=True,
                                prefix='<|startoftext|>',
                                truncate='<|endoftext|>',
                                top_k=40
                                )
    for dream in fresh_dreams:
      dream = dream.strip('<|startoftext|>').strip('</|endoftext|>')
      archive_dream(dream)
      rootLogger.debug(f"Dream:\n{dream}")
      if good(dream) and len(good_dreams) < DREAM_THRESHOLD:
        rootLogger.info(f"Good dream found:\n{dream}")
        good_dreams.append(dream)
        fp = save_image(dream)
      res = {'errors'}
      attempts = 0
      if len(dream) <= 250:
        while attempts <= RETRY_LIMIT and 'errors' in res:
          rootLogger.info(f"Attempting to send dream with length {len(dream)} to Twitter")
          res = api.PostDirectMessage(user_id=os.environ.get("USER_ID"), text=dream, return_json=True)
          attempts += 1
      else:
        fp = save_image(dream)
        while attempts <= RETRY_LIMIT and 'errors' in res:
          rootLogger.info(f"Attempting to send dream with length {len(dream)} to Twitter")
          res = api.PostDirectMessage(text='dream_image', user_id=os.environ.get("USER_ID"), media_file_path=fp, media_type='dm_image', return_json=True)
          attempts += 1
      rootLogger.debug(f"Twitter Direct Messages API response: {res}")    

if __name__ == "__main__":
    dream()
    teardown()
